package javaSwingExercise;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

public class Main implements ActionListener {
	JFrame window = new JFrame();

	JLabel lastNameLabel, firstNameLabel, genderLabel, birthDateLabel;
	JTextField lastNameField, firstNameField, birthDateField;
	JComboBox genderField;
	JButton addButton, saveButton, deleteButton;
	DefaultTableModel tableModel;
	JTable dataTable;
	JScrollPane scrollPane;

	String gender[] = { "Male", "Female" };
	String columns[] = { "ID", "Last Name", "First Name", "Gender", "Birth date"};
	
	int id = 1;

	Main() {
		// Labels
		lastNameLabel = new JLabel("Last Name: ");
		lastNameLabel.setBounds(10, 10, 100, 30);

		firstNameLabel = new JLabel("First Name: ");
		firstNameLabel.setBounds(10, 40, 100, 30);

		genderLabel = new JLabel("Gender: ");
		genderLabel.setBounds(10, 70, 100, 30);

		birthDateLabel = new JLabel("Birthdate: ");
		birthDateLabel.setBounds(10, 100, 100, 30);

		window.add(lastNameLabel);
		window.add(firstNameLabel);
		window.add(genderLabel);
		window.add(birthDateLabel);

		// Inputs
		lastNameField = new JTextField();
		lastNameField.setBounds(110, 10, 200, 30);
		lastNameField.setEnabled(false);

		firstNameField = new JTextField();
		firstNameField.setBounds(110, 40, 200, 30);
		firstNameField.setEnabled(false);

		genderField = new JComboBox(gender);
		genderField.setBounds(110, 70, 200, 30);
		genderField.setEnabled(false);

		birthDateField = new JTextField();
		birthDateField.setBounds(110, 100, 200, 30);
		birthDateField.setEnabled(false);

		window.add(lastNameField);
		window.add(firstNameField);
		window.add(genderField);
		window.add(birthDateField);

		// Controls
		addButton = new JButton("Add");
		addButton.setBounds(10, 140, 95, 40);
		addButton.setBackground(Color.green);

		saveButton = new JButton("Save");
		saveButton.setBounds(110, 140, 95, 40);
		saveButton.setBackground(Color.cyan);
		saveButton.setEnabled(false);

		deleteButton = new JButton("Delete");
		deleteButton.setBounds(210, 140, 95, 40);
		deleteButton.setBackground(Color.red);
		deleteButton.setEnabled(false);

		window.add(addButton);
		window.add(saveButton);
		window.add(deleteButton);
		
		// Table
		tableModel = new DefaultTableModel(columns, 0);
		dataTable = new JTable(tableModel);
		scrollPane = new JScrollPane(dataTable);
		scrollPane.setMinimumSize(new Dimension(480, 200));
		scrollPane.setPreferredSize(new Dimension(480, 200));
		scrollPane.setBounds(10, 200, 500, 200);
		
		window.add(scrollPane);
		
		addButton.addActionListener(this);
		saveButton.addActionListener(this);
		deleteButton.addActionListener(this);
		
		window.setSize(1000, 500);
		window.setLayout(null);
		window.setVisible(true);
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		if (arg0.getSource() == addButton) {
			saveButton.setEnabled(true);
			lastNameField.setEnabled(true);
			firstNameField.setEnabled(true);
			genderField.setEnabled(true);
			birthDateField.setEnabled(true);
		} else if (arg0.getSource() == saveButton) {
			tableModel.addRow(new Object[] { Integer.toString(id), lastNameField.getText(), firstNameField.getText(),
					genderField.getSelectedItem().toString(), birthDateField.getText() });
			
			lastNameField.setText("");
			firstNameField.setText("");
			genderField.setSelectedIndex(0);
			birthDateField.setText("");
			
			saveButton.setEnabled(false);
			lastNameField.setEnabled(false);
			firstNameField.setEnabled(false);
			genderField.setEnabled(false);
			birthDateField.setEnabled(false);
			
			id++;
		} else if (arg0.getSource() == deleteButton) {
			tableModel.removeRow(dataTable.getSelectedRow());
			deleteButton.setEnabled(false);
		}
		
		dataTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent arg0) {
				deleteButton.setEnabled(true);
				
			}
		});
	}

	public static void main(String[] args) {
		new Main();
	}
}
